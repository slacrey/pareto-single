package com.mashangwan.paleto.common;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * ErrorCode
 *
 * @author linfeng
 * @since 2017/7/30
 */
public enum ErrorCode {
    GLOBAL(2),

    AUTHENTICATION(10), JWT_TOKEN_EXPIRED(11);

    private int errorCode;

    private ErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    @JsonValue
    public int getErrorCode() {
        return errorCode;
    }
}
