package com.mashangwan.paleto.common.web;

import java.io.Serializable;

/**
 * com.seelyn.superp.core.endpoint.model
 *
 * @author linfeng
 * @since 2017/7/30
 */
public class View<T> implements Serializable {

    private int code;
    private String message;
    private T data;

    public View(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
