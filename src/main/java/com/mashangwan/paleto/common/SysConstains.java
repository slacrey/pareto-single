package com.mashangwan.paleto.common;


/**
 * 系统级常量
 *
 * @author lc
 * @date 2017年6月26日
 */
public class SysConstains {


    /**
     * 系统默认字符编码
     */
    public static final String SYSTEM_CHARSET = "UTF-8";
    public static final String GENERAL_USER = "general";


}
