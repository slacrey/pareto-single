package com.mashangwan.paleto.common.exception;


import com.mashangwan.paleto.common.exception.enums.ExceptionEnums;

/**
 * @描述: 参数验证异常
 * @作者: lc.
 * @创建时间: 2017/6/26 .
 * @版本号: V1.0 .
 */
public class PaletoValidateException extends PaletoException {

    private static final long serialVersionUID = -61934935032317234L;

    public PaletoValidateException(ExceptionEnums exceptionEnums) {
        super(exceptionEnums);
    }

    public PaletoValidateException(ExceptionEnums exceptionEnums, Throwable cause) {
        super(exceptionEnums, cause);
    }

    public PaletoValidateException(String message) {
        super(message);
    }

    public PaletoValidateException(String message, Throwable cause) {
        super(message, cause);
    }
}
