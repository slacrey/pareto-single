package com.mashangwan.paleto.common.exception;


import com.mashangwan.paleto.common.exception.enums.ExceptionEnums;

/**
 * @描述: 系统业务处理异常
 * @作者: lc.
 * @创建时间: 2017/6/26 .
 * @版本号: V1.0 .
 */
public class PaletoHandlerException extends PaletoException {
    private static final long serialVersionUID = -5600078259171877139L;

    public PaletoHandlerException(String message) {
        super(message);
    }

    public PaletoHandlerException(String message, Throwable cause) {
        super(message, cause);
    }

    public PaletoHandlerException(ExceptionEnums exceptionEnums) {
        super(exceptionEnums);
    }

    public PaletoHandlerException(ExceptionEnums exceptionEnums, Throwable cause) {
        super(exceptionEnums, cause);
    }

    public PaletoHandlerException(String message, int errorCode, Throwable cause) {
        super(message, errorCode, cause);
    }

    public PaletoHandlerException(String message, int errorCode) {
        super(message, errorCode);
    }

}
