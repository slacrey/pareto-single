package com.mashangwan.paleto.common.exception.enums;

/**
 * @描述: 业务参数错误异常信息
 * @作者: lc.
 * @创建时间: 2017/6/26 .
 * @版本号: V1.0 .
 */
public enum ParamExceptionEnums implements ExceptionEnums {

    INVALID_ACCOUNT(20001, "账户格式错误"),

    
    END(99999, "END");

    public int code;
    public String message;

    ParamExceptionEnums(final int code, final String message) {
        this.code = code;
        this.message = message;
    }

    ParamExceptionEnums(final String message) {
        this.message = message;
    }

    @Override
    public final int getCode() {
        return code;
    }

    @Override
    public final String getMessage() {
        return message;
    }
}
