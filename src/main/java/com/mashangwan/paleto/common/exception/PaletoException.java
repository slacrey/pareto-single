package com.mashangwan.paleto.common.exception;


import com.mashangwan.paleto.common.exception.enums.ExceptionEnums;

/**
 * @描述: 异常基类
 * @作者: lc.
 * @创建时间: 2017/6/26 .
 * @版本号: V1.0 .
 */
public class PaletoException extends RuntimeException {

    private static final long serialVersionUID = 9008026268539823228L;
    private ExceptionEnums exceptionEnums;
    private int errorCode = 500;

    /**
     * .
     *
     * @param exceptionEnums 异常
     */
    public PaletoException(final ExceptionEnums exceptionEnums) {
        super(exceptionEnums.getMessage());
        this.exceptionEnums = exceptionEnums;
        errorCode = exceptionEnums.getCode();
    }

    /**
     * .
     *
     * @param exceptionEnums 异常
     * @param cause          异常
     */
    public PaletoException(final ExceptionEnums exceptionEnums, final Throwable cause) {
        super(exceptionEnums.getMessage(), cause);
        this.exceptionEnums = exceptionEnums;
        errorCode = exceptionEnums.getCode();
    }

    public PaletoException(String message) {
        super(message);
    }

    public PaletoException(String message, Throwable cause) {
        super(message, cause);
    }

    public PaletoException(String message, int errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public PaletoException(String message, int errorCode, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public ExceptionEnums getExceptionEnums() {
        return exceptionEnums;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

}
