package com.mashangwan.paleto.common.exception.enums;

/**
 *
 * @描述: 系统内容异常信息
 * @作者: lc.
 * @创建时间: 2017/6/26 .
 * @版本号: V1.0 .
 */
public enum HandlerExceptionEnums implements ExceptionEnums {

    HANDLER_RATELIMITER(10001, "服务器繁忙"),
    HANDLER_SERVER_ERROR(10002, "服务器错误"),
    HANDLER_SERVER_DB(10003, "数据库操作失败");

    public int code;
    public String message;

    private HandlerExceptionEnums(final int code, final String message) {
        this.code = code;
        this.message = message;
    }

    private HandlerExceptionEnums(final String message) {
        this.message = message;
    }

    @Override
    public final int getCode() {
        return code;
    }

    @Override
    public final String getMessage() {
        return message;
    }
}
