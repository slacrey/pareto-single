package com.mashangwan.paleto.common.exception.enums;

/**
 *
 * @描述: 异常枚举
 * @作者: lc.
 * @创建时间: 2017/6/26 .
 * @版本号: V1.0 .
 */
public interface ExceptionEnums {

    public int getCode();

    public String getMessage();

}
