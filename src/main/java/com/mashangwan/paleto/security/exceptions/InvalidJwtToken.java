package com.mashangwan.paleto.security.exceptions;

/**
 * InvalidJwtToken
 *
 * @author linfeng
 * @since 2017/7/30
 */
public class InvalidJwtToken extends RuntimeException {
    private static final long serialVersionUID = -294671188037098603L;
}
