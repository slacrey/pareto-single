package com.mashangwan.paleto.security.exceptions;

import org.springframework.security.authentication.AuthenticationServiceException;

/**
 * AuthMethodNotSupportedException
 *
 * @author linfeng
 * @since 2017/7/30
 */
public class AuthMethodNotSupportedException extends AuthenticationServiceException {
    private static final long serialVersionUID = 3705043083010304496L;

    public AuthMethodNotSupportedException(String msg) {
        super(msg);
    }
}
