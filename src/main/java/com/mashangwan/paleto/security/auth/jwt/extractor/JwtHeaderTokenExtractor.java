package com.mashangwan.paleto.security.auth.jwt.extractor;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Component;

/**
 * JwtHeaderTokenExtractor
 *
 * @author linfeng
 * @since 2017/7/30
 */
@Component
public class JwtHeaderTokenExtractor implements TokenExtractor {
    public static String HEADER_PREFIX = "Bearer ";

    @Override
    public String extract(String header) {
        if (StringUtils.isBlank(header)) {
            throw new AuthenticationServiceException("Authorization header cannot be blank!");
        }

        if (header.length() < HEADER_PREFIX.length()) {
            throw new AuthenticationServiceException("Invalid authorization header size.");
        }

        if (!header.startsWith(HEADER_PREFIX)) {
            return header;
        }

        return header.substring(HEADER_PREFIX.length(), header.length());
    }
}
