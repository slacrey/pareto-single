package com.mashangwan.paleto.security.auth.jwt.extractor;

/**
 * TokenExtractor
 *
 * @author linfeng
 * @since 2017/7/30
 */
public interface TokenExtractor {

    public String extract(String payload);

}
