package com.mashangwan.paleto.security.auth.ajax;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 注册请求
 * com.seelyn.superp.core.security.endpoint
 *
 * @author linfeng
 * @since 2017/8/6
 */
public class RegisterRequest {

    private String username;
    private String password;

    @JsonCreator
    public RegisterRequest(@JsonProperty("username") String username, @JsonProperty("password") String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
