package com.mashangwan.paleto.security.auth.jwt.verifier;

/**
 * TokenVerifier
 *
 * @author linfeng
 * @since 2017/7/30
 */
public interface TokenVerifier {
    public boolean verify(String jti);
}
