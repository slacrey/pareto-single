package com.mashangwan.paleto.security;

import com.mashangwan.paleto.common.JsonUtils;
import com.mashangwan.paleto.common.SysConstains;
import com.mashangwan.paleto.common.exception.PaletoException;
import com.mashangwan.paleto.common.exception.PaletoHandlerException;
import com.mashangwan.paleto.common.exception.PaletoValidateException;
import com.mashangwan.paleto.common.exception.enums.HandlerExceptionEnums;
import com.mashangwan.paleto.common.web.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 全局异常处理.
 *
 * @author lc
 * @date 2017年6月26日
 */
@ControllerAdvice(basePackages = "com.seelyn.superp")
public class GlobalExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * appKey安全处理
     *
     * @param exception {EcpValidateException}
     * @param response  {HttpServletResponse}
     */
    @ExceptionHandler(PaletoValidateException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void validateExceptionResponse(PaletoValidateException exception, HttpServletResponse response) {

        logger.error("全局异常:{}", exception.getMessage(), exception);

        View<String> view = new View<>(exception.getErrorCode(), exception.getMessage());
        responseJson(response, view);
    }

    @ExceptionHandler(PaletoHandlerException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void handlerExceptionResponse(PaletoHandlerException exception, HttpServletResponse response) {

        logger.error("全局异常:{}", exception.getMessage(), exception);

        responseJson(response, new View<>(exception.getErrorCode(), exception.getMessage()));

    }

    @ExceptionHandler(PaletoException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void ecpExceptionResponse(PaletoException exception, HttpServletResponse response) {

        logger.error("全局异常:{}", exception.getMessage(), exception);

        responseJson(response, new View<>(exception.getErrorCode(), exception.getMessage()));

    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void exceptionResponse(Exception exception, HttpServletResponse response) {

        logger.error("全局异常:{}", exception.getMessage(), exception);

        PaletoException ecpException = new PaletoException(exception.getMessage(), HandlerExceptionEnums.HANDLER_SERVER_ERROR.getCode());

        responseJson(response, new View<>(ecpException.getErrorCode(), ecpException.getMessage()));

    }

    /**
     * 响应json格式字符串
     *
     * @param response {HttpServletResponse}
     * @param view     {View<String>}
     */
    private void responseJson(HttpServletResponse response, View<String> view) {
        response.setCharacterEncoding(SysConstains.SYSTEM_CHARSET);
        response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);

        try (PrintWriter out = response.getWriter()) {

            String result = JsonUtils.nonEmptyMapper().toJson(view);
            out.append(result);

        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

}
