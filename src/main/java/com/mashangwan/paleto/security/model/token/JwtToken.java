package com.mashangwan.paleto.security.model.token;

/**
 * JwtToken
 *
 * @author linfeng
 * @since 2017/7/30
 */
public interface JwtToken {
    String getToken();
}
