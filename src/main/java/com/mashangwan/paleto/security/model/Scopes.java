package com.mashangwan.paleto.security.model;

/**
 * Scopes
 *
 * @author linfeng
 * @since 2017/7/30
 */
public enum Scopes {
    REFRESH_TOKEN;
    
    public String authority() {
        return "ROLE_" + this.name();
    }
}
