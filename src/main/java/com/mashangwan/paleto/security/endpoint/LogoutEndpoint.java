package com.mashangwan.paleto.security.endpoint;

import com.seelyn.superp.sys.endpoint.model.View;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * com.seelyn.superp.core.security.endpoint
 *
 * @author linfeng
 * @since 2017/7/30
 */
@RestController
public class LogoutEndpoint {

    @RequestMapping(value = "/api/auth/logout", method = RequestMethod.GET)
    public View<String> logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return new View<>(200, "OK");
    }

}
