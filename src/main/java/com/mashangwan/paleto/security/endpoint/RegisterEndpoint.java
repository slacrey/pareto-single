package com.mashangwan.paleto.security.endpoint;

import com.seelyn.superp.sys.common.SysConstains;
import com.seelyn.superp.sys.domain.Duties;
import com.seelyn.superp.sys.domain.User;
import com.seelyn.superp.sys.endpoint.model.View;
import com.seelyn.superp.sys.security.auth.ajax.RegisterRequest;
import com.seelyn.superp.sys.service.DutiesService;
import com.seelyn.superp.sys.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 注册
 * com.seelyn.superp.core.security.endpoint
 *
 * @author linfeng
 * @since 2017/8/6
 */
@RestController
public class RegisterEndpoint {

    @Autowired
    private UserService userService;
    @Autowired
    private DutiesService dutiesService;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @RequestMapping(value = "/api/auth/register")
    public View<User> registerUser(@Validated @RequestBody RegisterRequest register) {

        String pwd = encoder.encode(register.getPassword());
        register.setPassword(pwd);
        User user = new User(register);

        Duties duties = dutiesService.findPositionByEnName(SysConstains.GENERAL_USER);
        if (duties != null) {
            user.getDutiesList().add(duties);
        }
        userService.save(user);
        View<User> view = new View<>(200, "OK");
        view.setData(user);
        return view;
    }

    @RequestMapping(value = "/api/auth/verification")
    public View<String> verificationCode(String mobile) {
        return new View<>(200, "OK");
    }

}
