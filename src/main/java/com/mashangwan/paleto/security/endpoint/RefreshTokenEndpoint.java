package com.mashangwan.paleto.security.endpoint;


import com.seelyn.superp.sys.domain.User;
import com.seelyn.superp.sys.endpoint.model.View;
import com.seelyn.superp.sys.security.auth.jwt.extractor.TokenExtractor;
import com.seelyn.superp.sys.security.auth.jwt.verifier.TokenVerifier;
import com.seelyn.superp.sys.security.config.JwtSettings;
import com.seelyn.superp.sys.security.config.WebSecurityConfig;
import com.seelyn.superp.sys.security.exceptions.InvalidJwtToken;
import com.seelyn.superp.sys.security.model.UserContext;
import com.seelyn.superp.sys.security.model.token.JwtToken;
import com.seelyn.superp.sys.security.model.token.JwtTokenFactory;
import com.seelyn.superp.sys.security.model.token.RawAccessJwtToken;
import com.seelyn.superp.sys.security.model.token.RefreshToken;
import com.seelyn.superp.sys.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * RefreshTokenEndpoint
 *
 * @author linfeng
 * @since 2017/7/30
 */
@RestController
public class RefreshTokenEndpoint {
    @Autowired
    private JwtTokenFactory tokenFactory;
    @Autowired
    private JwtSettings jwtSettings;
    @Autowired
    private UserService userService;
    @Autowired
    private TokenVerifier tokenVerifier;
    @Autowired
    @Qualifier("jwtHeaderTokenExtractor")
    private TokenExtractor tokenExtractor;

    @RequestMapping(value = "/api/auth/token", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    View<JwtToken> refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String tokenPayload = tokenExtractor.extract(request.getHeader(WebSecurityConfig.JWT_TOKEN_HEADER_PARAM));

        RawAccessJwtToken rawToken = new RawAccessJwtToken(tokenPayload);
        RefreshToken refreshToken = RefreshToken.create(rawToken, jwtSettings.getTokenSigningKey()).orElseThrow(() -> new InvalidJwtToken());

        String jti = refreshToken.getJti();
        if (!tokenVerifier.verify(jti)) {
            throw new InvalidJwtToken();
        }

        String subject = refreshToken.getSubject();
        User user = userService.getByUsername(subject).orElseThrow(() -> new UsernameNotFoundException("User not found: " + subject));

        if (user.getDutiesList() == null || user.getDutiesList().isEmpty())
            throw new InsufficientAuthenticationException("User has no position assigned");
        List<GrantedAuthority> authorities = user.getDutiesList().stream()
                .map(authority -> new SimpleGrantedAuthority(authority.authority()))
                .collect(Collectors.toList());

        UserContext userContext = UserContext.create(user.getUsername(), authorities);

        JwtToken jwtToken = tokenFactory.createAccessJwtToken(userContext);
        View<JwtToken> tokenView = new View<>(200, "200");
        tokenView.setData(jwtToken);
        return tokenView;
    }
}
