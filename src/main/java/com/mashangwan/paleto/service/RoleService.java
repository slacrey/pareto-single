package com.mashangwan.paleto.service;

import com.mashangwan.paleto.domain.Role;

import java.util.List;

/**
 * @author slacrey
 * @since 2018/1/14
 */
public interface RoleService {

    List<Role> findRoles(Long userId);

}
