package com.mashangwan.paleto.service;

import com.mashangwan.paleto.domain.User;

/**
 * @author slacrey
 * @since 2018/1/14
 */
public interface UserService {

    User findByLoginName(String loginName);

    void updateUserLoginInfo(String ip, Long userId);

}
