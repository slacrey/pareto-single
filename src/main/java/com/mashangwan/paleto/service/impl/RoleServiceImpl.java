package com.mashangwan.paleto.service.impl;

import com.mashangwan.paleto.domain.Role;
import com.mashangwan.paleto.domain.RoleExample;
import com.mashangwan.paleto.domain.UserRoleKey;
import com.mashangwan.paleto.mapper.RoleMapper;
import com.mashangwan.paleto.service.RoleService;
import com.mashangwan.paleto.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author slacrey
 * @since 2018/1/14
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private UserRoleService userRoleService;

    @Override
    public List<Role> findRoles(Long userId) {


        List<UserRoleKey> userRoleKeyList = userRoleService.findUserRoleKeys(userId);
        List<Long> roleIds = userRoleKeyList.stream().map(item -> item.getRoleId()).collect(Collectors.toList());

        RoleExample example = new RoleExample();
        RoleExample.Criteria criteria = example.createCriteria();
        criteria.andIdIn(roleIds);
        return roleMapper.selectByExample(example);
    }

}
