package com.mashangwan.paleto.service.impl;

import com.mashangwan.paleto.domain.UserRoleExample;
import com.mashangwan.paleto.domain.UserRoleKey;
import com.mashangwan.paleto.mapper.UserRoleMapper;
import com.mashangwan.paleto.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author slacrey
 * @since 2018/1/14
 */
@Service
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Override
    public List<UserRoleKey> findUserRoleKeys(Long userId) {

        UserRoleExample example = new UserRoleExample();
        example.createCriteria().andUserIdEqualTo(userId);
        return userRoleMapper.selectByExample(example);
    }
}
