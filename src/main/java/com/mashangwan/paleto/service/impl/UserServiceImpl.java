package com.mashangwan.paleto.service.impl;

import com.mashangwan.paleto.domain.User;
import com.mashangwan.paleto.domain.UserExample;
import com.mashangwan.paleto.mapper.UserMapper;
import com.mashangwan.paleto.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author slacrey
 * @since 2018/1/14
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User findByLoginName(String loginName) {


        UserExample example = new UserExample();
        example.createCriteria().andLoginNameEqualTo(loginName);
        List<User> users = userMapper.selectByExample(example);
        if (users.isEmpty()) {
            return null;
        }
        return users.get(0);
    }

    @Override
    public void updateUserLoginInfo(String ip, Long userId) {
        User record = new User();
        record.setId(userId);
        record.setLoginIp(ip);
        userMapper.updateByPrimaryKeySelective(record);
    }
}
