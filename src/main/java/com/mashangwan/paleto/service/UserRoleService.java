package com.mashangwan.paleto.service;

import com.mashangwan.paleto.domain.UserRoleKey;

import java.util.List;

/**
 * @author slacrey
 * @since 2018/1/14
 */
public interface UserRoleService {

    List<UserRoleKey> findUserRoleKeys(Long userId);

}
